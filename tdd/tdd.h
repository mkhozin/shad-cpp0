#pragma once

#include <string>
#include <cctype>
#include <memory>

class Tokenizer {
public:
    Tokenizer(std::istream* in) : in_(in) {}

    enum TokenType {
        UNKNOWN, //
        NUMBER,
        SYMBOL,
        END
    };

    void Consume() {
        type_ = UNKNOWN;
    }

    TokenType GetType() {
        if (type_ == UNKNOWN) {
            ReadNext();
        }
        return type_;
    }
    int64_t GetNumber() {
        if (type_ == UNKNOWN) {
            ReadNext();
        }
        return number_;
    }
    
    char GetSymbol() {
        if (type_ == UNKNOWN) {
            ReadNext();
        }
        return symbol_;
    }

private:
    std::istream* in_;

    TokenType type_ = TokenType::UNKNOWN;
    int64_t number_;
    char symbol_;

    void ReadNext() {
        *in_ >> std::ws;
        if (std::isdigit(in_->peek())) {
            *in_ >> number_;
            type_ = NUMBER;
        } else if(in_->peek() == EOF) {
            type_ = END;
        } else {
            symbol_ = in_->get();
            type_ = SYMBOL;
        }
    }
};

class Expression {
public:
    virtual int64_t Evaluate() = 0;

    virtual ~Expression() {}
};

class UnaryMinus : public Expression {
public:
    UnaryMinus(std::unique_ptr<Expression> child)
        : child_(std::move(child)) {}

    int64_t Evaluate() { return -child_->Evaluate(); }
private:
    std::unique_ptr<Expression> child_;
};

class Plus : public Expression {
public:
    Plus(
        std::unique_ptr<Expression> lhs,
        std::unique_ptr<Expression> rhs)
        : lhs_(std::move(lhs)), rhs_(std::move(rhs)) {}

    int64_t Evaluate() {
        return lhs_->Evaluate() + rhs_->Evaluate();
    }
private:
    std::unique_ptr<Expression> lhs_;
    std::unique_ptr<Expression> rhs_;
};

class Minus : public Expression {
public:
    Minus(
        std::unique_ptr<Expression> lhs,
        std::unique_ptr<Expression> rhs)
        : lhs_(std::move(lhs)), rhs_(std::move(rhs)) {}

    int64_t Evaluate() {
        return lhs_->Evaluate() - rhs_->Evaluate();
    }
private:
    std::unique_ptr<Expression> lhs_;
    std::unique_ptr<Expression> rhs_;
};
class Mul : public Expression {
public:
    Mul(
        std::unique_ptr<Expression> lhs,
        std::unique_ptr<Expression> rhs)
        : lhs_(std::move(lhs)), rhs_(std::move(rhs)) {}

    int64_t Evaluate() {
        return lhs_->Evaluate() - rhs_->Evaluate();
    }
private:
    std::unique_ptr<Expression> lhs_;
    std::unique_ptr<Expression> rhs_;
};
class Div : public Expression {
public:
    Div(
        std::unique_ptr<Expression> lhs,
        std::unique_ptr<Expression> rhs)
        : lhs_(std::move(lhs)), rhs_(std::move(rhs)) {}

    int64_t Evaluate() {
        return lhs_->Evaluate() - rhs_->Evaluate();
    }
private:
    std::unique_ptr<Expression> lhs_;
    std::unique_ptr<Expression> rhs_;
};

class Constant : public Expression {
public:
    Constant(int64_t value) : value_(value) {}

    int64_t Evaluate() { return value_; }

private:
    int64_t value_;
};

std::unique_ptr<Expression> ParseExpression(Tokenizer* tok) {
    if (tok->GetType() == Tokenizer::SYMBOL) {
        switch(tok->GetSymbol()) {
        case '-': {
            tok->Consume();
            return std::make_unique<UnaryMinus>(
                ParseExpression(tok));
        }
        }
    } else if (tok->GetType() == Tokenizer::NUMBER) {
        auto node = std::make_unique<Constant>(
            tok->GetNumber());
        tok->Consume();
        return node;
    }
}

std::unique_ptr<Expression> ParseFactor(Tokenizer* tok) {
    auto factor = ParseExpression(tok);

    while (tok->GetType() == Tokenizer::SYMBOL) {
        if (tok->GetSymbol() == '*') {
            tok->Consume();
            factor = std::make_unique<Mul>(
                std::move(factor),
                ParseExpression(tok));
        }
    }

    return factor;
}

std::unique_ptr<Expression> ParseTerm(Tokenizer* tok) {
    auto term = ParseFactor(tok);
    while (tok->GetType() == Tokenizer::SYMBOL) {
        if (tok->GetSymbol() == '+') {
            tok->Consume();
            term = std::make_unique<Plus>(
                std::move(term),
                ParseFactor(tok));
        } else {
            tok->Consume();
            term = std::make_unique<Minus>(
                std::move(term),
                ParseFactor(tok));
        }
    }

    return term;
}

std::unique_ptr<Expression> Parse(Tokenizer* tok) {
    return ParseTerm(tok);
}

int64_t Evaluate(const std::string& expression);
